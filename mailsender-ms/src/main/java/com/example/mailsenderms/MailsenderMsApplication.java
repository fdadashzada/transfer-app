package com.example.mailsenderms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailsenderMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailsenderMsApplication.class, args);
    }

}
